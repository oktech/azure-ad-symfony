<?php

namespace App\Service\Security;

use App\Entity\User;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\RedirectResponse;

interface OAuth2ServiceInterface
{
    public function connect(): RedirectResponse;
    public function check(): ?ResourceOwnerInterface;
    public function getUserByToken(AccessToken $accessToken, OAuth2ClientInterface $client): User;
}
