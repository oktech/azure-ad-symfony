<?php

namespace App\Service\Security\Azure;

use App\Entity\User;
use App\Service\Security\OAuth2ServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\RedirectResponse;
use TheNetworg\OAuth2\Client\Provider\Azure;
use TheNetworg\OAuth2\Client\Provider\AzureResourceOwner;

class AzureAdOAuth2Service implements OAuth2ServiceInterface
{

    const OAUTH2_TYPE = 'azure';
    private const OAUTH2_SCOPES = [
        'email'
    ];
    private const OAUTH2_OPTIONS = [
        'email'
    ];

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ClientRegistry $clientRegistry,
    ){}

    public function connect(): RedirectResponse
    {
        return $this->clientRegistry
            ->getClient(self::OAUTH2_TYPE)
            ->redirect(
                scopes: self::OAUTH2_SCOPES,
                options: self::OAUTH2_OPTIONS
            );
    }

    public function check(): ?ResourceOwnerInterface
    {
        $client = $this->clientRegistry->getClient(self::OAUTH2_TYPE);

        try {

            return $client->fetchUser();
        } catch (\Throwable $e) {
            return null;
        }
    }

    public function getUserByToken(AccessToken $accessToken, OAuth2ClientInterface $client): User
    {
        /** @var AzureResourceOwner $azureUser */
        $azureUser = $client->fetchUserFromToken($accessToken);

        dd($azureUser);

        $existingUser = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $azureUser->getUpn()]);

        if ($existingUser) {
            return $existingUser;
        }

        return $this->getOrCreateUser($azureUser);
    }

    /**
     * @param AzureResourceOwner $azureUser
     * @return User
     */
    public function getOrCreateUser(AzureResourceOwner $azureUser): User
    {
        $user = new User();
        $user->setFirstName($azureUser->getFirstName())
            ->setLastName($azureUser->getLastName())
            ->setEmail($azureUser->getUpn());

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
