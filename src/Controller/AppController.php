<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/app', name: 'app_')]
class AppController extends AbstractController
{
    #[Route(path: '/login', name: 'login')]
    public function loginPage(): Response
    {
        return $this->render('login.html.twig');
    }

    #[Route('/logout', name: 'logout')]
    public function logout(): JsonResponse
    {
        return $this->json('Logout successfully');
    }

    #[Route(path: '/home', name: 'home_page', methods: [Request::METHOD_GET])]
    #[IsGranted('ROLE_USER')]
    public function homepage(): Response
    {
        return $this->render('home.html.twig');
    }
}