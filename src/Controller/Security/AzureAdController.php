<?php

namespace App\Controller\Security;

use App\Service\Security\Azure\AzureAdOAuth2Service;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/azure/connect', name: 'azure_connect_')]
class AzureAdController extends AbstractController
{
    public function __construct(
        private readonly AzureAdOAuth2Service $azureAdOAuth2Service,
    ){}

    #[Route(path: '/', name: 'start')]
    public function connect(): RedirectResponse
    {
        return $this->azureAdOAuth2Service->connect();
    }

    #[Route(path: '/check', name: 'check')]
    public function check(): JsonResponse
    {
        return $this->json($this->azureAdOAuth2Service->check());
    }
}
