<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class DefaultController extends AbstractController
{

    #[Route(path: '/healthcheck', name: 'app_healthcheck', methods: [Request::METHOD_GET])]
    public function homepage(): Response
    {
        return $this->json([
            'api_status' => 'OK',
            'api_name' => $this->getParameter('app.name'),
            'api_version' => $this->getParameter('app.version'),
        ]);
    }
}
